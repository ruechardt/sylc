const express = require('express')
const path = require('path')
const fs = require('fs')
const vueServerRenderer = require('vue-server-renderer')
const request = require('request')

const port = 80
const app = express()

const axios = require('axios')

const instance = axios.create({
    // baseURL: 'http://sylc-wirvsvirus.de/api/',
    baseURL: 'http://localhost:3001/',
})

const getShop = id => instance.get('/shops/' + id)
const getShops = () => instance.get('/shops')

const createRenderer = (bundle) =>
    vueServerRenderer.createBundleRenderer(bundle, {
        runInNewContext: false,
        template: fs.readFileSync(path.resolve(__dirname, 'index.html'), 'utf-8')
    })
let renderer

// you may want to serve static files with nginx or CDN in production
app.use('/public',  express.static(path.resolve(__dirname, './dist')))

if (process.env.NODE_ENV === 'development') {
    // require will fail in production
    const setupDevServer = require('./config/setup-dev-server')
    setupDevServer(app, (serverBundle) => {
        renderer = createRenderer(serverBundle)
    })
} else {
    renderer = createRenderer(require('./dist/vue-ssr-server-bundle.json'))
}

const renderAndSend = async (context, req, res) => {

    let html
    try {
        html = await renderer.renderToString(context)
    } catch (error) {
        if (error.code === 404) {
            return res.status(404).send('404 | Page Not Found')
        }
        console.error(error)
        return res.status(500).send('500 | Internal Server Error')
    }

    res.end(html)

}

app.get(/^\/(about)?\/?$/, async (req, res) => {

    const shops = (await getShops()).data

    const context = {
        url: req.params['0'] || '/',
        state: {
            shops,
            shop: null,
            filter: '',
        }
    }
    await renderAndSend(context, req, res)
    
})

app.get('/shop/:shopId', async (req, res) => {

    const shop = (await getShop(req.params.shopId)).data

    const context = {
        url: '/shop/' + req.params.shopId,
        state: {
            shops: [],
            shop,
            filter: '',
        }
    }
    await renderAndSend(context, req, res)

})

app.get('/enlist', async (req, res) => {

    const context = {
        url: '/enlist',
        state: {
            shops: [],
            shop: null,
            filter: '',
        }
    }
    await renderAndSend(context, req, res)

})

app.get('/faq', async (req, res) => {

    const context = {
        url: '/faq',
        state: {
            shops: [],
            shop: null,
            filter: '',
        }
    }
    await renderAndSend(context, req, res)

})

app.get('/api/shops', (req, res) => {
    const newurl = 'http://localhost:3001/shops'
    req.pipe(request({ qs: req.query, uri: newurl })).pipe(res);

})

app.get('/api/shops/:id', (req, res) => {
    const newurl = 'http://localhost:3001/shops/' + req.params.id
    req.pipe(request(newurl)).pipe(res)
})

app.post('/api/shops', (req, res) => {
    const newurl = 'http://localhost:3001/shops'
    req.pipe(request(newurl)).pipe(res)
})

app.listen(port, () => console.log(`Listening on: ${port}`))