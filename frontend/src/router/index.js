import Vue from 'vue'
import Router from 'vue-router'
import About from '../modules/about/index.vue'
import Home from '../modules/home/index.vue'
import Shop from '../modules/shop/index.vue'
import Enlist from '../modules/enlist/index.vue'
import FAQ from '../modules/faq/index.vue'

Vue.use(Router)

export function createRouter () {
    return new Router({
        mode: 'history',
        routes: [
            {
                path: '/',
                component: Home,
                name: 'home'
            },
            {
                path: '/about',
                component: About,
                name: 'about'
            },
            {
                path: '/shop/:id',
                component: Shop,
                name: 'shop',
            },
            {
                path: '/enlist',
                component: Enlist,
                name: 'enlist',
            },
            {
                path: '/faq',
                component: FAQ,
                name: 'faq',
            },
        ]
    })
}
