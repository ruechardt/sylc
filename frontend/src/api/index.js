import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://sylc-wirvsvirus.de/api/',
    // baseURL: 'http://localhost:3000/api/',
})

const getShops = () => instance.get('/shops')
const filterShops = zip => instance.get('/shops?zip=' + zip)
const getShop = id => instance.get('/shops/' + id)
const createShop = shop => instance.post('/shops/', shop)

export default {
    getShops,
    filterShops,
    getShop,
    createShop,
}