export const title = state => state.title

export const shops = state => state.shops
export const shop = state => state.shop
export const filter = state => state.filter
