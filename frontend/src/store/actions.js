import api from '../api'

export const getShops = ({ commit }) => {
    commit('setFilter', '')
    api.getShops()
        .then(response => commit('setShops', response.data))
}

export const filterShops = ({ commit }, zip) => {
    commit('setFilter', zip)
    api.filterShops(zip)
        .then(response => commit('setShops', response.data))
}

export const getShop = ({ commit }, id) => {
    api.getShop(id)
        .then(response => commit('setShop', response.data))
}

export const createShop = ({ commit }, shop) => {
    return api.createShop(shop)
        .then(response => {
            commit('setShop', response.data)
        })
}
