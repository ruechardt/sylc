export const setShops = (state, shops) => {
    state.shops = shops
}

export const setShop = (state, shop) => {
    state.shop = shop
}

export const setFilter = (state, filter) => {
    state.filter = filter
}
