from glob import glob
import requests
import json
from tqdm import tqdm

class Api:

    def __init__(self, host='localhost', port='3001'):
        self.host = host
        self.port = port

    def post_shop(self, data):
        response = requests.post(f'http://{self.host}:{self.port}/shops', json=data)
        assert response.status_code == 201, f'Expected status for POST /shops got {response.status_code} \n {response.text}'


api_client = Api()

for shop_path in tqdm(glob('shops/*.json')):
    with open(shop_path, 'rt') as f:
        print(f)
        data = json.load(f)
    api_client.post_shop(data)