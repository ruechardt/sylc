import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from './config/config.module'
import { MongooseModule } from '@nestjs/mongoose'
import { ShopsModule } from './shops/shops.module'
import { ImagesModule } from './images/images.module';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    ConfigModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useExisting: ConfigService
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useExisting: ConfigService,
    }),
    ShopsModule,
    ImagesModule,
  ],
})
export class AppModule {}
