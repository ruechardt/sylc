import {
    prop,
    getModelForClass,
    buildSchema,
    mongoose,
    DocumentType,
  } from '@typegoose/typegoose'
  
  export class ImageInstance {
    _id!: mongoose.Types.ObjectId
  
    @prop({ required: true })
    filename!: string

    @prop({required: true})
    mimetype!: string

    @prop({required: true})
    url!: string
  }
  
  export const imageModel = getModelForClass(ImageInstance)
  export const imageSchema = buildSchema(ImageInstance)
  export type imageDocument = DocumentType<ImageInstance>
  