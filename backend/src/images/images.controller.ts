import { Controller, Post, UseInterceptors, UploadedFile } from '@nestjs/common';
import {FileInterceptor} from '@nestjs/platform-express'
import { ImagesService } from './images.service';
import * as path from 'path'

@Controller('images')
export class ImagesController {

    constructor(private readonly imagesService: ImagesService) {}

    @Post('/')
    @UseInterceptors(FileInterceptor('image'))
    async uploadFile(@UploadedFile() file: any) {
        const newFilePath = await this.imagesService.saveUpload(path.extname(file.originalname), file.buffer)
    }

}
