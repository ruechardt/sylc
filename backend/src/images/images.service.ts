import { Injectable } from '@nestjs/common';
import * as fs from 'fs'
import { v1 } from 'uuid'

//using local file system for now
@Injectable()
export class ImagesService {

    saveUpload(fileExtension : string, buffer: Buffer) {
        const name = `${v1()}${fileExtension}`
        return new Promise(
            (resolve, reject) => {
                fs
                    .createWriteStream(fileExtension)
                    .once('open', async (fd: any) => {
                        fs.write(fd, buffer, (err: any) => {
                            if(err) {
                                reject(err)
                            }
                            fs.close(fd, (err: any) => {
                                if(err) {
                                    reject(err)
                                }
                                resolve()
                            })
                        })
                    })
                    .on('error', reject)
            }
        )
       
    }

}
