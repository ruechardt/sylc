import { NestFactory, Reflector } from '@nestjs/core'
import { AppModule } from './app.module'
import { ConfigService } from './config/config.module'
import {
  INestApplication,
  ValidationPipe
} from '@nestjs/common'
import { LoggingInterceptor } from './common/logging.interceptor'

export function setupApp(app: INestApplication): INestApplication {
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true
    })
  )

  app.useGlobalInterceptors(new LoggingInterceptor())
  
  return app
}

export async function bootstrap() {
  const app: INestApplication = setupApp(await NestFactory.create(AppModule))
  await app.listen(app.get(ConfigService).SERVER_PORT)
}
