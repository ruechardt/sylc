import {
  prop,
  getModelForClass,
  buildSchema,
  mongoose,
  DocumentType,
  arrayProp,
} from '@typegoose/typegoose'

export class ShopInstance {
  _id!: mongoose.Types.ObjectId

  @prop({ required: true })
  title!: string

  @prop({required: true})
  address!: string

  @prop({required: true})
  zip!: string

  @prop({required: true})
  needInCent!: number

  @prop()
  paypalUrl?: string

  @prop()
  thumbnailUrl?: string

  @prop()
  websiteUrl?: string

  @prop()
  facebookUrl?: string

  @prop()
  instagramUrl?: string

  @prop()
  story?: string

  @prop()
  offer?: string

  @arrayProp({type: String})
  imageUrls?: string[]

}

export const shopModel = getModelForClass(ShopInstance)
export const shopSchema = buildSchema(ShopInstance)
export type shopDocument = DocumentType<ShopInstance>
