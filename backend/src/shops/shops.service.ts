import { Injectable, HttpException, HttpStatus } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { ShopInstance, shopDocument } from './schemas/shop.schema'
import { ReturnModelType } from '@typegoose/typegoose'
import { ShopDTO } from './dtos/shop.dto'

export interface IFindAllQuerryParameters {
  zip: string 
}

@Injectable()
export class ShopsService {
  constructor(
    @InjectModel(ShopInstance.name)
    private readonly shopModel: ReturnModelType<typeof ShopInstance>,
  ) {}

  async getShopByIdOrFail(id: string): Promise<shopDocument> {
    const returnedShop = await this.shopModel.findById(id)
    if (!returnedShop) {
      throw new HttpException(
        `Shop with id ${id} does not exist`,
        HttpStatus.NOT_FOUND,
      )
    }
    return returnedShop
  }

  async getAllShops(limit: number, queryParams: Partial<IFindAllQuerryParameters> = {}): Promise<shopDocument[]> {
    const query: any = {}
    if(queryParams.zip != null) {
      query.zip = queryParams.zip
    }
    return await this.shopModel.find(query).limit(limit)
  }

  async createShop(shop: ShopDTO): Promise<shopDocument> {
    return await this.shopModel.create(shop)
  }
}
