import { Module } from '@nestjs/common'
import { ShopsController } from './shops.controller'
import { ShopsService } from './shops.service'
import { MongooseModule } from '@nestjs/mongoose'
import { ShopInstance, shopSchema } from './schemas/shop.schema'

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ShopInstance.name,
        schema: shopSchema,
      },
    ]),
  ],
  controllers: [ShopsController],
  providers: [ShopsService],
})
export class ShopsModule {}
