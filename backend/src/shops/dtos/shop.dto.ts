import { IsString, IsInt, IsArray, ValidateNested, IsOptional } from 'class-validator'
import { prop } from '@typegoose/typegoose'
import { Type } from 'class-transformer'

export class ShopDTO {
  @IsString()
  title!: string

  @IsString()
  address!: string

  @IsString()
  zip!: string

  @IsInt()
  needInCent!: number

  @IsOptional()
  thumbnailUrl?: string

  @IsOptional()
  paypalUrl?: string

  @IsOptional()
  websiteUrl?: string

  @IsOptional()
  facebookUrl?: string

  @IsOptional()
  instagramUrl?: string

  @IsOptional()
  story?: string

  @IsOptional()
  offer?: string

  @IsString({each: true})
  imageUrls?: string[]
}
