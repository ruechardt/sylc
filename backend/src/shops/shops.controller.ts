import { Controller, Get, Param, Post, Body, Query } from '@nestjs/common'
import { ShopsService } from './shops.service'
import { ShopDTO } from './dtos/shop.dto'

@Controller('shops')
export class ShopsController {
  constructor(private readonly shopsService: ShopsService) {}

  @Get('/')
  async getAllShops(
    @Query('zip') zip?: string
  ) {
    const allShops = await this.shopsService.getAllShops(20, {
      zip
    })
    return allShops.map(shop => shop.toJSON())
  }

  @Post('/')
  async createShop(@Body() shop: ShopDTO) {
    const createdShop = await this.shopsService.createShop(shop)
    return createdShop.toJSON()
  }

  @Get('/:shopId')
  async getShopById(@Param('shopId') shopId: string) {
    const shop = await this.shopsService.getShopByIdOrFail(shopId)
    return shop.toJSON()
  }
}
