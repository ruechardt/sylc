import { Module } from '@nestjs/common'
import {
  IsInt,
  IsString,
  IsIn,
  Min,
  IsOptional,
  IsNumber,
} from 'class-validator'
import { transformAndValidateSync } from 'class-transformer-validator'
import { Type } from 'class-transformer'
import { MongooseModuleOptions, MongooseOptionsFactory } from '@nestjs/mongoose'
import { MulterOptionsFactory, MulterModuleOptions } from '@nestjs/platform-express'

export class ConfigService implements MongooseOptionsFactory, MulterOptionsFactory {
  @IsInt()
  @Min(80)
  readonly SERVER_PORT = 3000

  @IsIn(['development', 'production', 'test'])
  readonly NODE_ENV!: string // use enums here

  @IsIn(['error', 'warn', 'info', 'debug', 'test'])
  readonly LOG_LEVEL!: string // use enums here

  @IsOptional()
  readonly USE_JWT?: boolean = false

  @IsString()
  readonly MONGO_HOST!: string

  @IsString()
  readonly MONGO_DATABASE!: string

  @IsNumber()
  @Type(() => Number)
  readonly MONGO_PORT: number = 27017

  @IsString()
  @IsOptional()
  readonly MONGO_USER?: string

  @IsString()
  @IsOptional()
  readonly MONGO_PASSWORD?: string

  @IsString()
  UPLOAD_DIR = '../files'

  isProduction() {
    return this.NODE_ENV === 'production'
  }

  createMongooseOptions(): MongooseModuleOptions {
    return {
      uri: `mongodb://${this.MONGO_HOST}:${this.MONGO_PORT}/${this.MONGO_DATABASE}`,
      user: this.MONGO_USER,
      pass: this.MONGO_PASSWORD,
    }
  }

  createMulterOptions(): MulterModuleOptions {
    return {
      dest: `${__dirname}/../${this.UPLOAD_DIR}`,
    }
  }

  static fromPlainObject(object: Object = process.env): ConfigService {
    try {
      const configService = transformAndValidateSync(this, object, {
        validator: {
          whitelist: true,
          forbidUnknownValues: true,
        },
      })

      return configService
    } catch (errors) {
      throw new Error(`Invalid Config \n \n ${errors.join('\n')}`)
    }
  }
}

@Module({
  providers: [
    {
      provide: ConfigService,
      useValue: ConfigService.fromPlainObject(),
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule {}
