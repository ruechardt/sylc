FROM ubuntu

RUN  apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash \
    && apt-get install nodejs -yq

RUN npm i jest-cli -g

WORKDIR /usr/src/app

COPY *.json ./

RUN npm i

CMD ["npm", "run", "start:dev" ]
